<?php phpinfo(); exit;?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<h2>File Upload</h2>
	<p class="lead"></p>

	<!-- Upload  -->
	<form id="file-upload-form" class="uploader" action="upload.php" onchange="javascript:this.submit()" method="POST" enctype="multipart/form-data">
		<input id="file-upload" type="file" name="fileUpload" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />

		<label for="file-upload" id="file-drag">
			<div id="start">
				<i class="fa fa-download" aria-hidden="true"></i>
				<div>Select a file or drag here</div>
				<div id="notimage" class="hidden">Please select an image</div>
				<span id="file-upload-btn" class="btn btn-primary">Select a file</span>
			</div>
		</label>
	</form>

	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script src="assets/js/script.js"></script>
</body>
</html>
