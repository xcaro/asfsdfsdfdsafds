<?php
require_once('import.php');
if (isset($_FILES['fileUpload'])) {
	try {

    // Undefined | Multiple Files | $_FILES Corruption Attack
    // If this request falls under any of them, treat it invalid.
		if (
			!isset($_FILES['fileUpload']['error']) ||
			is_array($_FILES['fileUpload']['error'])
		) {
			throw new RuntimeException('Invalid parameters.');
		}

    // Check $_FILES['fileUpload']['error'] value.
		switch ($_FILES['fileUpload']['error']) {
			case UPLOAD_ERR_OK:
			break;
			case UPLOAD_ERR_NO_FILE:
			throw new RuntimeException('No file sent.');
			case UPLOAD_ERR_INI_SIZE:
			case UPLOAD_ERR_FORM_SIZE:
			throw new RuntimeException('Exceeded filesize limit.');
			default:
			throw new RuntimeException('Unknown errors.');
		}

    // You should also check filesize here. 
		if ($_FILES['fileUpload']['size'] > 1000000) {
			throw new RuntimeException('Exceeded filesize limit.');
		}

    // DO NOT TRUST $_FILES['fileUpload']['mime'] VALUE !!
    // Check MIME Type by yourself.
		$finfo = new finfo(FILEINFO_MIME_TYPE);
		if (false === $ext = array_search(
			$finfo->file($_FILES['fileUpload']['tmp_name']),
			['xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
			true
		)) {
			throw new RuntimeException('Invalid file format.');
		}

    // You should name it uniquely.
    // DO NOT USE $_FILES['fileUpload']['name'] WITHOUT ANY VALIDATION !!
    // On this example, obtain safe unique name from its binary data.
		if (!move_uploaded_file(
			$_FILES['fileUpload']['tmp_name'],
			sprintf('./input.xlsx',
				sha1_file($_FILES['fileUpload']['tmp_name'])
			)
		)) {
			throw new RuntimeException('Failed to move uploaded file.');
		}

		import_file();

	} catch (RuntimeException $e) {
		die($e->getMessage());
	}
}